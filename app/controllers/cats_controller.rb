class CatsController < ApplicationController
  
  def index
    @cats = Cat.all
    @tasks = Task.all
  end
  
  def show
    @cat =Cat.find(params[:id])
    @Tasks = @cat.tasks.paginate(page: params[:page])
    @task  = Task.new
  end
  
  def new
    @cat = Cat.new
  end
  
  def create
    @cat = Cat.new(cat_params)    
    if @cat.save
      flash[:success] = "Category Created"
      redirect_to @cat
    else
      render 'new'
    end
  end
  
  def edit
    @cat = Cat.find(params[:id])
  end
  
  def update
   @cat = Cat.find(params[:id])
    if @cat.update(cat_params)
      flash[:success] = "Category Edited"
      redirect_to @cat
    else
      render 'edit'
    end
  end
  
  def destroy
    Cat.find(params[:id]).destroy
    flash[:success] = "Category Deleted"
    redirect_to '/'
  end
  
  private
  
    def cat_params
      params.require(:cat).permit(:name)
    end
end
