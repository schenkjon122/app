class TasksController < ApplicationController
  
  def show
     @task = Task.find(params[:id])
  end
  
  def new
      @task=Task.new
  end
  
  
  def create
    @task=Task.new(task_params)
    if @task.save
      flash[:success] = "task created!"
      if @task.done == true
          @task.cat_id =2
          @task.save
          redirect_to '/cats/2'
      else
          @task.cat_id =1
          @task.save
          redirect_to '/cats/1'
      end
    else
      render '/'
    end
  end
  
  def edit
    @task = Task.find(params[:id])
  end
  
  def update
    @task = Task.find(params[:id])
    if @task.update(task_params)
      flash[:success] = "Task Edited"
      if @task.done == true
          @task.cat_id =2
          @task.save
          redirect_to '/cats/2'
      else
          @task.cat_id =1
          @task.save
          redirect_to '/cats/1'
      end
    else
      render 'edit'
    end
  end

  def destroy
    Task.find(params[:id]).destroy
    flash[:success] = "Category Deleted"
    redirect_to '/'
  end
  
  private

    def task_params
      params.require(:task).permit(:name, :need, :fix, :issue, :done, :cat_id, :subject )
    end
end
