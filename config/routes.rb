Rails.application.routes.draw do
  root 'cats#index'
  resources :cats
  resources :tasks
end
