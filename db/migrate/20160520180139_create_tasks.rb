class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :need
      t.string :fix
      t.string :issue
      t.boolean :done
      t.references :cat, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :tasks, [:cat_id, :created_at]
  end
end
